package com.example.retropract.network

import com.example.retropract.model.Post
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {
    @GET("posts/1")
    fun getPost(): Call<List<Post>>
}