package com.example.retropract.network

import com.example.retropract.util.Constants.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RestClient {
    val apiInterface: ApiInterface
    get() {
        val client = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return client.create(ApiInterface::class.java)
    }
}